/*
Copyright (c) 2012-2017 thomas.zink_at_uni-konstanz_dot_de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#ifndef _HANDLER_H_
#define _HANDLER_H_

#include <pcap.h>			// pcap packet capturing

/* 
 handlers for the different layers
 we could do this all in one function actually
 and prevent function call overhead.
 however, this would decrease readability , generality and maintainability
 greatly.
 */
void ethernet_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * packet);
void chdlc_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * packet);
void ip_handler (u_char * args, const struct pcap_pkthdr *header, const u_char * ippacket);

void print_payload(const u_char *payload, int len);
void print_payload_to_file(const u_char *payload, int len, FILE *file);
void print_hex_ascii_line(const u_char *payload, int len, int offset);
#endif // _HANDLER_H_
