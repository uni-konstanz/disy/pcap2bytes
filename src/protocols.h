/*
Copyright (c) 2012-2017 thomas.zink_at_uni-konstanz_dot_de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#ifndef _PROTOCOLS_H_
#define _PROTOCOLS_H_

#define __FAVOR_BSD // this is to make use of bsd style protocol structs

#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

// typedefs for convenience and consistency
typedef struct ether_header ether_t;
typedef struct ip ip_t;
typedef struct tcphdr tcp_t;
typedef struct udphdr udp_t;

/*
	CHDLC
*/
#define CHDLC_HDRLEN 		4
#define CHDLC_UNICAST		0x0f
#define CHDLC_BCAST			0x8f
#define CHDLC_TYPE_SLARP 	0x8035
#define CHDLC_TYPE_CDP		0x2000
#define CHDLC_TYPE_IP		0x0800

typedef struct chdlc_hdr_s {
	u_char	address;
	u_char	control;
	u_short code;
} chdlc_t;
/* CHDLC */



#endif