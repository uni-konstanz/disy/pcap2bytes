/*
Copyright (c) 2012-2017 thomas.zink_at_uni-konstanz_dot_de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#ifndef _DEFINES_H_
#define _DEFINES_H_

#define APP_NAME		"pcap2byte"
#define AUTHOR			"Thomas Zink"
#define EMAIL				"thomas.zink@uni-konstanz.de"
#define TASK				"exports pcap packet payload to binary file"

/*
 DEBUG and ASSERT
*/
#define DEBUG 1
#define ASSERT 0

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

#endif // _DEFINES_H_
