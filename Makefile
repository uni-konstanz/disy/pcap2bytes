CC = gcc
FLAGS = -Wall -O2
INC = -I./include/
LIB = -lpcap
SRC = ./src/*.c
BIN = pcap2bytes

all:
	$(CC) $(FLAGS) $(INC) $(SRC) -o $(BIN) $(LIB) 

clean:
	rm -rf $(BIN)
